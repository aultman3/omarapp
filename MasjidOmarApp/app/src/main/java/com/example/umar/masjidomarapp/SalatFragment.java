package com.example.umar.masjidomarapp;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.Calendar;
import java.util.Locale;

import android.app.AlarmManager;
import android.widget.Toast;

/**
 * Created by Umar on 1/19/2016.
 */
public class SalatFragment extends Fragment {








    public int FAJR_IQAMA_IN_MINUTES=6;
    public int DHUHR_IQAMA_IN_MINUTES;
    public int ASR_IQAMA_IN_MINUTES;
    public int MAGHRIB_IQAMA_IN_MINUTES;
    public int ISHA_IQAMA_IN_MINUTES;

    JSONArray jTimes;

    PendingIntent pendingIntent;
    AlarmManager setAlarm;
    TextView date, nextIqama, fajrAthen, dhuhrAthen, asrAthen, maghribAthen, ishaAthen,
            fajrIqama, dhuhrIqama, asrIqama, maghribIqama, ishaIqama;
    SalatTimeDB salatTimeDB;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (!prefs.getBoolean("firstTime", false)) {
            // <---- run your one time code here
            getContext().startService(new Intent(getContext(), SetAlarmService.class));

            setAlarm = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(getContext(), SetAlarmService.class);
            pendingIntent = PendingIntent.getService(getContext(), 0, intent, 0);
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 3);
            c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH)+1);
            setAlarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);


            // mark first time has runned.
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstTime", true);
            editor.commit();
        }




    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.salat_fragment, container, false);
        init(view);
        setup();

return view;
    }

    private void init(View view) {
        salatTimeDB = new SalatTimeDB(this.getContext());

        nextIqama = (TextView) view.findViewById(R.id.next_iqama);
        date = (TextView) view.findViewById(R.id.date);
        dhuhrAthen = (TextView) view.findViewById(R.id.dhuhr_athen);
        fajrAthen = (TextView) view.findViewById(R.id.fajr_athen);
        asrAthen = (TextView) view.findViewById(R.id.asr_athen);
        maghribAthen = (TextView) view.findViewById(R.id.maghrib_athen);
        ishaAthen = (TextView) view.findViewById(R.id.isha_athen);
        fajrIqama = (TextView) view.findViewById(R.id.fajr_iqama);
        dhuhrIqama = (TextView) view.findViewById(R.id.dhuhr_iqama);
        asrIqama = (TextView) view.findViewById(R.id.asr_iqama);
        maghribIqama = (TextView) view.findViewById(R.id.maghrib_iqama);
        ishaIqama = (TextView) view.findViewById(R.id.isha_iqama);


        salatTimeDB.add("1", "Mon", "22/4", "6:25", "6:40 AM", "7:41", "12:46", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:28", "4:00 PM", "5:50", "6:00 PM", "7:07", "7:30 PM");

        salatTimeDB.add("2", "Tue", "23", "6:24", "6:40 AM", "7:41", "12:46", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:29", "4:00 PM", "5:51", "6:00 PM", "7:08", "7:30 PM");

        salatTimeDB.add("3", "Wed", "24", "6:22", "6:40 AM", "7:39", "12:46", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:31", "4:00 PM", "5:54", "6:00 PM", "7:11", "7:30 PM");

        salatTimeDB.add("4", "Thu", "25", "6:21", "6:40 AM", "7:38", "12:47", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:32", "4:00 PM", "5:55", "6:00 PM", "7:12", "7:30 PM");

        salatTimeDB.add("5", "Fri", "26", "6:20", "6:40 AM", "7:37", "12:47", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:33", "4:00 PM", "5:56", "6:05 PM", "7:13", "7:30 PM");

        salatTimeDB.add("6", "Sat", "27", "6:19", "6:40 AM", "7:36", "12:47", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:34", "4:00 PM", "5:57", "6:05 PM", "7:14", "7:30 PM");

        salatTimeDB.add("7", "Sun", "28", "6:19", "6:40 AM", "7:34", "12:47", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:35", "4:00 PM", "5:58", "6:05 PM", "7:15", "7:30 PM");

        salatTimeDB.add("8", "Mon", "29", "6:18", "6:40 AM", "7:33", "12:47", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:36", "4:00 PM", "6:00", "6:05 PM", "7:16", "7:30 PM");

        salatTimeDB.add("9", "Tue", "30", "6:17", "6:40 AM", "7:32", "12:47", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:37", "4:00 PM", "6:01", "6:10 PM", "7:17", "7:30 PM");

        salatTimeDB.add("10", "Wed", "1", "6:16", "6:40 AM", "7:31", "12:47", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:38", "4:00 PM", "6:02", "6:10 PM", "7:18", "7:30 PM");

        salatTimeDB.add("11", "Thu", "2", "6:14", "6:30 AM", "7:30", "12:47", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:39", "4:00 PM", "6:03", "6:10 PM", "7:19", "7:40 PM");

        salatTimeDB.add("12", "Fri", "3", "6:13", "6:30 AM", "7:29", "12:47", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:40", "4:00 PM", "6:04", "6:10 PM", "7:20", "7:40 PM");

        salatTimeDB.add("13", "Sat", "4", "6:12", "6:30 AM", "7:28", "12:47", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:41", "4:00 PM", "6:06", "6:10 PM", "7:21", "7:40 PM");

        salatTimeDB.add("14", "Sun", "5", "6:11", "6:30 AM", "7:26", "12:47", "1:00 PM",
                "Second Khotbah at 1:30 PM", "3:42", "4:00 PM", "6:07", "6:15 PM", "7:22", "7:40 PM");


    }





private void setup(){
    Calendar c = Calendar.getInstance();
    String d = c.getDisplayName(c.MONTH, c.LONG, Locale.US) + " "+ Integer.toString(c.get(c.DATE))+ ", " + Integer.toString(c.get(c.YEAR));

    date.setText(d);
    Cursor entry = salatTimeDB.returnEntry(c.get(c.DATE));
    entry.moveToFirst();

    String fAthen = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_FAJR_ATHEN));
    String fIqama = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_FAJR_IQAMA));
    String dAthen = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_DHUHR_ATHEN));
    String dIqama = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_DHUHR_IQAMA));
    String aAthen = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_ASR_ATHEN));
    String aIqama = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_ASR_IQAMA));
    String mAthen = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_MAGHRIB_ATHEN));
    String mIqama = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_MAGHRIB_IQAMA));
    String iAthen = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_ISHA_ATHEN));
    String iIqama = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_ISHA_IQAMA));


            fajrAthen.setText(fAthen + " AM");
    fajrIqama.setText(fIqama);
    dhuhrAthen.setText(dAthen + " PM");
    dhuhrIqama.setText(dIqama);
    asrAthen.setText(aAthen + " PM");
    asrIqama.setText(aIqama);
    maghribAthen.setText(mAthen + " PM");
    maghribIqama.setText(mIqama);
    ishaAthen.setText(iAthen + " PM");
    ishaIqama.setText(iIqama);

//Setup for the next Iqama textview
    int cTime = (c.get(c.HOUR_OF_DAY)*60)+c.get(c.MINUTE);
    int fTime=entry.getInt(entry.getColumnIndex(SalatTimeDB.COLUMN_FAJR_MINUTES)),
            dTime=entry.getInt(entry.getColumnIndex(SalatTimeDB.COLUMN_DHUHR_MINUTES)),
            aTime=entry.getInt(entry.getColumnIndex(SalatTimeDB.COLUMN_ASR_MINUTES)),
            mTime=entry.getInt(entry.getColumnIndex(SalatTimeDB.COLUMN_MAGHRIB_MINUTES)),
            iTime=entry.getInt(entry.getColumnIndex(SalatTimeDB.COLUMN_ISHA_MINUTES));
    if(cTime<=fTime){
        nextIqama.setText(fIqama);
    }else if(cTime>fTime&&cTime<=dTime){
        nextIqama.setText(dIqama);
    }else if(cTime>dTime&&cTime<=aTime){
        nextIqama.setText(aIqama);
    }else if(cTime>aTime&&cTime<=mTime){
        nextIqama.setText(mIqama);
    }else if(cTime>mTime&&cTime<=iTime){
        nextIqama.setText(iIqama);
    }else{
        entry.moveToNext();
        entry.moveToFirst();
        nextIqama.setText(entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_FAJR_IQAMA)));
    }


}



}


