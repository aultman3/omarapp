package com.example.umar.masjidomarapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;

/**
 * Created by Umar on 1/26/2016.
 */
public class IshaAlarmService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "com.example.umar.masjidomarapp.FajrAlarmService");
        wl.acquire();
        NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification n = new Notification.Builder(this)
                .setContentTitle("Masjid Omar")
                .setContentText("Isha in 10 Minutes")
                .setSmallIcon(R.mipmap.omar_launcher).build();
        n.defaults = Notification.DEFAULT_ALL;
        n.priority = Notification.PRIORITY_MAX;
        nManager.notify(0, n);
        wl.release();
        stopSelf();


        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}



