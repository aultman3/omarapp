package com.example.umar.masjidomarapp;

import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class SalatTimeDB extends SQLiteOpenHelper{
    public static final String TABLE_NAME = "Month";
    public static final String COLUMN_DAY_ENTRY_ID = "day";
    public static final String COLUMN_DAY_NAME = "dayname";
    public static final String COLUMN_HIJRI = "hijri";
    public static final String COLUMN_FAJR_ATHEN="fajrathen";
    public static final String COLUMN_FAJR_IQAMA="fajriqama";
    public static final String COLUMN_SUNRISE="sunrise";
    public static final String COLUMN_DHUHR_ATHEN="dhuhrathen";
    public static final String COLUMN_DHUHR_IQAMA="dhuhriqama";
    public static final String COLUMN_FRIDAY="friday";
    public static final String COLUMN_ASR_ATHEN="asrathen";
    public static final String COLUMN_ASR_IQAMA="asriqama";
    public static final String COLUMN_MAGHRIB_ATHEN="maghribathen";
    public static final String COLUMN_MAGHRIB_IQAMA="maghribiqama";
    public static final String COLUMN_ISHA_ATHEN="ishaathen";
    public static final String COLUMN_ISHA_IQAMA="ishaiqama";
    public static final String COLUMN_FAJR_MINUTES="fajrminutes";
    public static final String COLUMN_DHUHR_MINUTES="dhuhrminutes";
    public static final String COLUMN_ASR_MINUTES="asrminutes";
    public static final String COLUMN_MAGHRIB_MINUTES="maghribminutes";
    public static final String COLUMN_ISHA_MINUTES="ishaminutes";


    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public SalatTimeDB(Context context) {
        super(context, "SalatTimeDB" , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS SalatTimeDB");
        db.execSQL("CREATE TABLE SalatTimeDB(day integer PRIMARY KEY, dayname text, hijri text," +
                "fajrathen text, fajriqama text, sunrise text, dhuhrathen text, dhuhriqama text, friday text," +
                "asrathen text, asriqama text, maghribathen text, maghribiqama text, ishaathen text, ishaiqama text,"+
                "fajrminutes int, dhuhrminutes int, asrminutes int, maghribminutes int, ishaminutes int);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS SalatTimeDB");
        onCreate(db);
    }

public boolean add(String day, String dayName, String hijri, String fajrAthen, String fajrIqama ,
                   String sunrise, String dhuhrAthen, String dhuhrIqama, String friday,
                   String asrAthen, String asrIqama, String maghribAthen, String maghribIqama,
                   String ishaAthen, String ishaIqama){
    int stop = fajrIqama.indexOf(':');
    int fTime =  (Integer.parseInt(fajrIqama.substring(0, stop))*60)+
            Integer.parseInt(fajrIqama.substring(stop + 1, stop + 3));
    stop=dhuhrIqama.indexOf(':');
    int dTime =  Integer.parseInt(dhuhrIqama.substring(0, stop));
    if(dTime<10){dTime+=12;}
    dTime =  (dTime*60)+Integer.parseInt(dhuhrIqama.substring(stop + 1, stop + 3));

    stop=asrIqama.indexOf(':');

    int aTime =((Integer.parseInt(asrIqama.substring(0, stop))+12)*60)+
            Integer.parseInt(asrIqama.substring(stop + 1, stop + 3));


    stop=maghribIqama.indexOf(':');
    int mTime =  ((Integer.parseInt(maghribIqama.substring(0, stop))+12)*60)+
            Integer.parseInt(maghribIqama.substring(stop + 1, stop + 3));
    stop=ishaIqama.indexOf(':');
    int iTime =  Integer.parseInt(ishaIqama.substring(0, stop));
    if(iTime<10){iTime+=12;}
    iTime =  (iTime*60)+Integer.parseInt(ishaIqama.substring(stop + 1, stop + 3));
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues contentValues = new ContentValues();
    contentValues.put("day", Integer.parseInt(day));
    contentValues.put("dayname", dayName);
    contentValues.put("hijri", hijri);
    contentValues.put("fajrathen", fajrAthen);
    contentValues.put("fajriqama", fajrIqama);
    contentValues.put("sunrise", sunrise);
    contentValues.put("dhuhrathen", dhuhrAthen);
    contentValues.put("dhuhriqama", dhuhrIqama);
    contentValues.put("friday", friday);
    contentValues.put("asrathen", asrAthen);
    contentValues.put("asriqama", asrIqama);
    contentValues.put("maghribathen", maghribAthen);
    contentValues.put("maghribiqama", maghribIqama);
    contentValues.put("ishaathen", ishaAthen);
    contentValues.put("ishaiqama", ishaIqama);
    contentValues.put("fajrminutes", fTime);
    contentValues.put("dhuhrminutes", dTime);
    contentValues.put("asrminutes", aTime);
    contentValues.put("maghribminutes", mTime);
    contentValues.put("ishaminutes", iTime);
    db.insert("SalatTimeDB", null, contentValues);
    return true;
}

public boolean remove(Integer day){
    SQLiteDatabase db = this.getWritableDatabase();
    db.delete("SalatTimeDB", "day = ? ", new String[]{Integer.toString(day)});
    return true;
}


    public Cursor returnEntry(int day){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from SalatTimeDB where day=" + day, null);

        return cursor;
    }
}
