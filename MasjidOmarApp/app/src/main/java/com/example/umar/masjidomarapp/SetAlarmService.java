package com.example.umar.masjidomarapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by Umar on 2/1/2016.
 */
public class SetAlarmService extends Service {


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        AlarmManager fajrAlarm, dhuhrAlarm, asrAlarm, maghribAlarm, ishaAlarm;
        Intent fajrIntent, dhuhrIntent, asrIntent, maghribIntent, ishaIntent;
        PendingIntent fajrPending, dhuhrPending, asrPending, maghribPending, ishaPending;

        SalatTimeDB  salatTimeDB = new SalatTimeDB(getBaseContext());
        Calendar c = Calendar.getInstance();
        Cursor entry = salatTimeDB.returnEntry(c.get(c.DATE));
        entry.moveToFirst();

        String fAthen = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_FAJR_ATHEN))+"AM";
        String fIqama = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_FAJR_IQAMA));
        String dAthen = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_DHUHR_ATHEN))+"PM";
        String dIqama = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_DHUHR_IQAMA));
        String aAthen = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_ASR_ATHEN))+"PM";
        String aIqama = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_ASR_IQAMA));
        String mAthen = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_MAGHRIB_ATHEN))+"PM";
        String mIqama = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_MAGHRIB_IQAMA));
        String iAthen = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_ISHA_ATHEN))+"PM";
        String iIqama = entry.getString(entry.getColumnIndex(SalatTimeDB.COLUMN_ISHA_IQAMA));

        fajrIntent = new Intent(getApplicationContext(), FajrAlarmService.class);
        dhuhrIntent = new Intent(getApplicationContext(), DhuhrAlarmService.class);
        asrIntent = new Intent(getApplicationContext(), AsrAlarmService.class);
        maghribIntent = new Intent(getApplicationContext(), MaghribAlarmService.class);
        ishaIntent = new Intent(getApplicationContext(), IshaAlarmService.class);

        fajrAlarm =(AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        dhuhrAlarm =(AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        asrAlarm =(AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        maghribAlarm =(AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        ishaAlarm =(AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);

        fajrPending = PendingIntent.getService(getApplicationContext(), 0, fajrIntent, 0);
        dhuhrPending = PendingIntent.getService(getApplicationContext(), 0, dhuhrIntent, 0);
        asrPending = PendingIntent.getService(getApplicationContext(), 0, asrIntent, 0);
        maghribPending = PendingIntent.getService(getApplicationContext(), 0, maghribIntent, 0);
        ishaPending = PendingIntent.getService(getApplicationContext(), 0, ishaIntent, 0);


        int fajrMin = entry.getInt(entry.getColumnIndex(SalatTimeDB.COLUMN_FAJR_MINUTES))-10,
                dhuhrMin=entry.getInt(entry.getColumnIndex(SalatTimeDB.COLUMN_DHUHR_MINUTES))-10,
                asrMin=entry.getInt(entry.getColumnIndex(SalatTimeDB.COLUMN_ASR_MINUTES))-10,
                maghribMin=entry.getInt(entry.getColumnIndex(SalatTimeDB.COLUMN_MAGHRIB_MINUTES))-10,
                ishaMin=entry.getInt(entry.getColumnIndex(SalatTimeDB.COLUMN_ISHA_MINUTES))-10,
                curMin=(c.get(Calendar.HOUR_OF_DAY)*60)+c.get(Calendar.MINUTE);
        c.set(Calendar.SECOND,0);
        if(curMin<fajrMin) {
            c.set(Calendar.HOUR_OF_DAY, fajrMin / 60);
            c.set(Calendar.MINUTE, fajrMin % 60);
            fajrAlarm.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), AlarmManager.INTERVAL_DAY, fajrPending);
        }
        if(curMin<dhuhrMin) {
            c.set(Calendar.HOUR_OF_DAY, dhuhrMin / 60);
            c.set(Calendar.MINUTE, dhuhrMin % 60);
            dhuhrAlarm.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), AlarmManager.INTERVAL_DAY, dhuhrPending);
        }
        if(curMin<asrMin) {
            c.set(Calendar.HOUR_OF_DAY, asrMin / 60);
            c.set(Calendar.MINUTE, asrMin % 60);
            asrAlarm.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), AlarmManager.INTERVAL_DAY, asrPending);
        }
        if(curMin<maghribMin) {
            c.set(Calendar.HOUR_OF_DAY, maghribMin / 60);
            c.set(Calendar.MINUTE, maghribMin % 60);
            maghribAlarm.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), AlarmManager.INTERVAL_DAY, maghribPending);
        }
        if(curMin<ishaMin) {
            c.set(Calendar.HOUR_OF_DAY, ishaMin / 60);
            c.set(Calendar.MINUTE, ishaMin % 60);
            ishaAlarm.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), AlarmManager.INTERVAL_DAY, ishaPending);
        }
        stopSelf();
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
